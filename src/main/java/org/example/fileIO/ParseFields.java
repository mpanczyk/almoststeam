package org.example.fileIO;

public enum ParseFields {
	ID_CASE("1"),
	TITLE_CASE("2"),
	CATEGORY_CASE("3"),
	PUBLISHER_CASE("4"),
	PRICE_CASE("5");

	private final String charOption;
	ParseFields(final String charOption) {
		this.charOption = charOption;
	}

	public String getCharOption() {
		return charOption;
	}


}
