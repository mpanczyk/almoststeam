package org.example.fileIO;

import org.example.entity.Game;
import org.example.entity.GameStore;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;

import static org.example.fileIO.ConstantFilePaths.SEP;

public class FileInputStreamToBase {
	FileCreator fileCreator = new FileCreator();
	public void gameStorageStatus() {
		fileCreator.createFileIfNotExist(ConstantFilePaths.FILE_PATH_GAME_STORAGE_DATA);
		try (
				FileInputStream in = new FileInputStream(ConstantFilePaths.FILE_PATH_GAME_STORAGE_DATA
						.concat(SEP).concat(FilesName.GAME_STORAGE_FILE_NAME))
		) {
			fileCreator.createFileIfNotExist(ConstantFilePaths.ARCHIVE_FILE_PATH_GAME_STORAGE_DATA);
			FileOutputStream out = new FileOutputStream(ConstantFilePaths.ARCHIVE_FILE_PATH_GAME_STORAGE_DATA
					.concat(SEP).concat(FilesName.GAME_STORAGE_ARCHIVE_FILE_NAME));
			int c;
			while ((c = in.read()) != -1) {
				out.write(c);
			}
			out.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public void userGameStorageStatus() {
		fileCreator.createFileIfNotExist(ConstantFilePaths.FILE_PATH_USER_GAME_STORAGE_DATA);
		try (
				FileInputStream in = new FileInputStream(ConstantFilePaths.FILE_PATH_USER_GAME_STORAGE_DATA
						.concat(SEP).concat(FilesName.USER_GAME_STORAGE_FILE_NAME))
		) {
			fileCreator.createFileIfNotExist(ConstantFilePaths.ARCHIVE_FILE_PATH_USER_GAME_STORAGE_DATA);
			FileOutputStream out = new FileOutputStream(ConstantFilePaths.ARCHIVE_FILE_PATH_USER_GAME_STORAGE_DATA
					.concat(SEP).concat(FilesName.USER_GAME_STORAGE_ARCHIVE_FILE_NAME));
			int c;
			while ((c = in.read()) != -1) {
				out.write(c);
			}
			out.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public void walletUserStatus() {
		fileCreator.createFileIfNotExist(ConstantFilePaths.FILE_PATH_USER_WALLET_DATA);
		try (
				FileInputStream in = new FileInputStream(ConstantFilePaths.FILE_PATH_USER_WALLET_DATA
						.concat(SEP).concat(FilesName.USER_WALLET_FILE_NAME))
		) {
			fileCreator.createFileIfNotExist(ConstantFilePaths.ARCHIVE_FILE_PATH_USER_WALLET_DATA);
			FileOutputStream out = new FileOutputStream(ConstantFilePaths.ARCHIVE_FILE_PATH_USER_WALLET_DATA
					.concat(SEP).concat(FilesName.USER_WALLET_ARCHIVE_FILE_NAME));
			int c;
			while ((c = in.read()) != -1) {
				out.write(c);
			}
			out.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public void walletSteamStatus() {
		fileCreator.createFileIfNotExist(ConstantFilePaths.FILE_PATH_STEAM_WALLET_DATA);
		try (
				FileInputStream in = new FileInputStream(ConstantFilePaths.FILE_PATH_STEAM_WALLET_DATA
						.concat(SEP).concat(FilesName.STEAM_WALLET_FILE_NAME))
		) {
			fileCreator.createFileIfNotExist(ConstantFilePaths.ARCHIVE_FILE_PATH_STEAM_WALLET_DATA);
			FileOutputStream out = new FileOutputStream(ConstantFilePaths.ARCHIVE_FILE_PATH_STEAM_WALLET_DATA
					.concat(SEP).concat(FilesName.STEAM_WALLET_ARCHIVE_FILE_NAME));

			int c;
			while ((c = in.read()) != -1) {
				out.write(c);
			}
			out.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public void walletPublisherStatus() {
		fileCreator.createFileIfNotExist(ConstantFilePaths.FILE_PATH_PUBLISHER_WALLET_DATA);
		try (
				FileInputStream in = new FileInputStream(ConstantFilePaths.FILE_PATH_PUBLISHER_WALLET_DATA
						.concat(SEP).concat(FilesName.PUBLISHER_WALLET_FILE_NAME))
		) {
			fileCreator.createFileIfNotExist(ConstantFilePaths.ARCHIVE_FILE_PATH_PUBLISHER_WALLET_DATA);
			FileOutputStream out = new FileOutputStream(ConstantFilePaths.ARCHIVE_FILE_PATH_PUBLISHER_WALLET_DATA
					.concat(SEP).concat(FilesName.PUBLISHER_WALLET_ARCHIVE_FILE_NAME));
			int c;
			while ((c = in.read()) != -1) {
				out.write(c);
			}
			out.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public static void saveDataInBase(List<Game> games, String filePath) {
		/* dodać wskazanie na konkretny plik */
		try {
			String text = games.toString();
			String textWithoutCharacter_1 = text.replaceAll("]", "");
			Files.writeString(Paths.get(filePath), textWithoutCharacter_1, StandardOpenOption.CREATE);
		} catch (IOException exception) {
			exception.printStackTrace();
		}
	}

	public static void cleanFileCharacterFormula(String filePath) {
		final String strOne = "\\[";
		final String strTwo = "\n";
		final String strThree = ",";
		final String strLast = "]";

		try {
			String fileToConvert = Files.readString(Paths.get(filePath));
			String firstConvertion = fileToConvert.replaceAll(strOne, "");
			String secondConvertion = firstConvertion.replaceFirst(strTwo, "");
			String thirdConvertion = secondConvertion.replaceAll(strThree, "");
			String finalConvertion = thirdConvertion.replaceAll(strLast, "");

			Files.writeString(Path.of(filePath), finalConvertion);

		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	public static void printDate(String filePath) {
		/* dodać wskazanie na konkretny plik */
		try {
			List<String> file = Files.readAllLines(Paths.get(filePath));
			file.forEach(System.out::println);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void getNeutralDataFormat(String filePath) {
		try (BufferedReader in = new BufferedReader(new FileReader("user.txt"));
			 BufferedWriter out = new BufferedWriter(new FileWriter("buffered_user_output.txt")))
		{
			String line;
			while ((line = in.readLine()) != null) {
				out.write(line);
				out.append("\n");
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	static void copyFile() throws IOException {
		FileReader in = new FileReader("user2.txt");
		FileWriter out = new FileWriter("writer_user_output.txt");
		int nextChar;
		while ((nextChar = in.read()) != -1) {
			System.out.println("Pobrany znak: " + nextChar + " to: " + (char) nextChar);
			out.append((char) nextChar);
		}
		in.close();
		out.close();
	}

	public static void main(String[] args) {
		FileInputStreamToBase fistb = new FileInputStreamToBase();
		fistb.fileCreator.createFileIfNotExist(ConstantFilePaths.FILE_PATH_GAME_STORAGE_DATA);
		fistb.gameStorageStatus();
		GameStore gsf = new GameStore();
		saveDataInBase(gsf.getGameList(), ConstantFilePaths.FILE_PATH_GAME_STORAGE_DATA.concat(SEP).concat(FilesName.GAME_STORAGE_FILE_NAME));
		cleanFileCharacterFormula(ConstantFilePaths.FILE_PATH_GAME_STORAGE_DATA.concat(SEP).concat(FilesName.GAME_STORAGE_FILE_NAME));
		printDate(ConstantFilePaths.FILE_PATH_GAME_STORAGE_DATA.concat(SEP).concat(FilesName.GAME_STORAGE_FILE_NAME));
	}
}
