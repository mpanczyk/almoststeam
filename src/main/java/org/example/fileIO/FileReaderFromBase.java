package org.example.fileIO;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;


public class FileReaderFromBase implements DataTextSeparator {
	public final Scanner IN = new Scanner(System.in);
//	public String getFieldFromFile(String filePath, Scanner IN) {
//
//		do {
//
//			printApplicationControlMenu();
//
//			switch (IN.nextLine()) {
//				case "1":
//					break;
//				case "2":
//					break;
//				case "3":
//					break;
//				case "4":
//					break;
//				case "5":
//					break;
//				default:
//					System.out.println("Incorect chose");
//			}
//		} while ()
//	return "";
//	}

	public void printMain() {

	}
	public void showFileContents(String pathFile, String message) {
		try {
			BufferedReader reader = new BufferedReader(new FileReader(pathFile));

			String line;
			printMessage(message);

			while ((line = reader.readLine()) != null) {
				System.out.println(line);
			}
			reader.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public void printMessage(String message) {
		System.out.println(message);
	}
//	public void printApplicationControlMenu() {
//		System.out.println("Enter the keyword you want to search the library based on:"
//				.concat( "1. ID\n")
//				.concat("2. Title\n")
//				.concat("3. Category\n")
//				.concat("4. Publisher\n")
//				.concat("Select an option: "));
//	}
}