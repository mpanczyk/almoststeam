package org.example.entity;

import org.example.fileIO.DataTextSeparator;

public class Game implements DataTextSeparator {

    int id;
    String title;
    GameCategory category;
    String publisher;
    double price;


    public Game(int id, String title, GameCategory category, String publisher, double price) {
        this.id = id;
        this.title = title;
        this.category = category;
        this.publisher = publisher;
        this.price = price;

    }

    public int getId() {
        return id;
    }

    public double getPrice() {
        return price;
    }

    public String getTitle() {
        return title;
    }

    public String getPublisher() {
        return publisher;
    }

//    @Override
//    public String toString() {
//        return "\n\u001B[1m\u001B[32m" + title + "\u001B[0m\n" +
//                "Category: " + category + "\n" +
//                "Publisher: " + publisher + "\n" +
//                "Price: " + String.format("%.2f", price);
//    }

    @Override
    public String toString() {
        return  "\n" +
                id + DATA_TEXT_SEPARATOR +
                title + DATA_TEXT_SEPARATOR +
                category + DATA_TEXT_SEPARATOR +
                publisher + DATA_TEXT_SEPARATOR +
                price + DATA_TEXT_SEPARATOR;
    }
}
