package org.example.entity;

import java.util.HashMap;

public class Publisher {
    static HashMap<String, Double> revenue = new HashMap<>();

    public static void addRevenue(String publisher, double amount) {
        revenue.put(publisher, revenue.getOrDefault(publisher, 0.0) + amount);
    }
}
