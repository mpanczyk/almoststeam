package org.example.service;

import org.example.MainUser;
import org.example.entity.Game;
import org.example.entity.GameStore;
import org.example.entity.UserGameLibrary;
import org.example.entity.Wallet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import static org.example.entity.Wallet.getWallet;


public class App {
    private final Map<String, User> users = new HashMap<>();
    private final UserService userService = new UserService();
    GameStore gameStore = new GameStore();

    public void appMenu() {
        System.out.println("1. Game Library\n"
                .concat("2. Wallet\n")
                .concat("3. Game Store\n")
                .concat("4. Change Password\n")
                .concat("5. Logout\n")
                .concat("6. Delete account\n")
                .concat("Select an option: "));
    }

    public void userMenu(MainUser user, Scanner in) {
        boolean loggedIn = true;

        while (loggedIn) {
            appMenu();
            int choice = in.nextInt();
            in.nextLine();

            switch (choice) {
                case 1:
                    displayUserLibrary();
                    break;
                case 2:
                    handleWalletMenu(user, in);
                    break;
                case 3:
                    handleGameStore(in, user);
                    break;
                case 4:
                    userService.changePassword(user, in);
                    break;
                case 5:
                    logout();
                    return;
                case 6:
                    userService.deleteUserAccount(user, in);
                    loggedIn = false;
                    break;
                default:
                    System.out.println("Invalid choice. Please select a valid option");
                    break;
            }
        }
    }

    private void displayUserLibrary() {
        ArrayList<Game> userLibrary = UserGameLibrary.getUserGameLibrary();
        String message = userLibrary.isEmpty() ? "\n\nBuy some game if you don't want to see empty library\n\n"
                : "\u001B[31m\u001B[1m\nYour Game Library: \n\u001B[0m";
        System.out.println(message);

        for (Game game : userLibrary) {
            System.out.println(game + "\n");
        }
    }

    private void handleWalletMenu(MainUser user, Scanner in) {
        Wallet userWallet = getWallet(user.getUsername());
        userWallet.walletMenu(in, user.getUsername());
    }

    private void handleGameStore(Scanner in, MainUser user) {
        displayGameList();
        String gameChoice = in.nextLine().trim();

        if ("X".equalsIgnoreCase(gameChoice)) {
            return;
        }

        try {
            int gameChoiceInt = Integer.parseInt(gameChoice);
            gameStore.showGameMenu(gameChoiceInt, in, user);
        } catch (NumberFormatException e) {
            System.out.println("Invalid input, please enter a valid game ID or 'X' to exit.");
        }
    }

    private void displayGameList() {
        for (Game game : gameStore.getGameList()) {
            System.out.println(game.getId() +
                    ". \u001B[32m" + game.getTitle() +
                    "\u001B[0m - Price: " +
                    String.format("%.2f", game.getPrice()));
        }
        System.out.println("Choose game ID or 'X' to go back to the main menu.");
    }

    private void logout() {
        System.out.println("Logging out.\n");
        MainUser.startApp();
    }
}