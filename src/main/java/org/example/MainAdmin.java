package org.example;

import org.example.entity.Game;
import org.example.entity.GameStore;
import org.example.entity.SteamWallet;
import org.example.fileIO.ConstantFilePaths;
import org.example.fileIO.FileReaderFromBase;
import org.example.fileIO.FilesName;

import java.io.*;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import static org.example.fileIO.ConstantFilePaths.SEP;


public class MainAdmin {
    private static final String ADMIN_FILE = ConstantFilePaths.FILE_PATH_ADMIN_DATA.concat(SEP).concat(FilesName.ADMIN_FILE_NAME);
    private static final String USERS_LIST_FILE = ConstantFilePaths.FILE_PATH_USERS_LIST.concat(SEP).concat(FilesName.USERS_LIST_FILE_NAME);
    private static final String STORAGE_GAME_FILE = ConstantFilePaths.FILE_PATH_GAME_STORAGE_DATA.concat(SEP).concat(FilesName.GAME_STORAGE_FILE_NAME);
    static FileReaderFromBase fileReaderFromBase = new FileReaderFromBase();

    public static boolean login() {
        Scanner in = new Scanner(System.in);

        String storedUsername = "Admin";
        String storedPassword = "Admin1";

        if (!initCredentialsFile(storedUsername, storedPassword)) {
            System.out.println("Could not create admin credentials file.");
            return false;
        }

        if (!readCredentialsFromFile(storedUsername, storedPassword)) {
            System.out.println("Admin credentials file not found.");
            return false;
        }

        return verifyAdminLogin(in, storedUsername, storedPassword);
    }

    private static boolean initCredentialsFile(String username, String password) {
        File credentialsFile = new File(ADMIN_FILE);
        if (!credentialsFile.exists()) {
            try (PrintWriter writer = new PrintWriter(credentialsFile)) {
                writer.println(username);
                writer.println(password);
                return true;
            } catch (IOException e) {
                return false;
            }
        }
        return true;
    }

    private static boolean readCredentialsFromFile(String username, String password) {
        File credentialsFile = new File(ADMIN_FILE);
        try (Scanner scanner = new Scanner(credentialsFile)) {
            username = scanner.nextLine().trim();
            password = scanner.nextLine().trim();
            return true;
        } catch (FileNotFoundException e) {
            return false;
        }
    }

    private static boolean verifyAdminLogin(Scanner in, String storedUsername, String storedPassword) {
        System.out.print("Enter admin username: ");
        String inputUsername = in.nextLine();
        System.out.print("Enter admin password: ");
        String inputPassword = in.nextLine();

        if (inputUsername.equals(storedUsername) && inputPassword.equals(storedPassword)) {
            System.out.println("Admin login successful.");
            return true;
        } else {
            return false;
        }
    }

    public static void adminMenu() {
        Scanner scanner = new Scanner(System.in);
        boolean running = true;

        while (running) {
            adminMainMenu();

            String choice = scanner.nextLine();

            switch (choice) {
                case "1":
                    fileReaderFromBase.showFileContents(USERS_LIST_FILE,
                            "\nList of all users: ");
                    break;
                case "2":
                    listAllGames();
//                    fileReaderFromBase.showFileContents(STORAGE_GAME_FILE,
//                            "\nList of all games from Steam: ");
                    break;
                case "3":
                    listGamePurchases(); //Add how many times single game was sold
                    break;
                case "4":
                    listAllPublishers(); //Add publisher balance to this method
                    break;
                case "5":
                    showSteamWalletBalance();
                    break;
                case "6":
                    running = false;
                    break;
                default:
                    System.out.println("Invalid choice.");
            }
        }
    }

    public static void adminMainMenu() {
        System.out.println("\nAdmin Menu:\n"
                .concat("1 - List all users\n")
                .concat("2 - List all games\n")
                .concat("3 - List game purchases... Don't Touch this!\n")
                .concat("4 - List all publishers and their balance\n")
                .concat("5 - Show steam account balance\n")
                .concat("6 - Exit\n"));
    }

    public static void listAllGames() {
        GameStore gameStore = new GameStore();
        List<Game> games = gameStore.getGameList();

        System.out.println("\nList of all available games:");
        for (Game game : games) {
            System.out.println(game.getId() + ". " + game.getTitle());
        }
    }

    public static void listGamePurchases() {
    }

    public static void listAllPublishers() {
        GameStore gameStore = new GameStore();
        List<Game> games = gameStore.getGameList();

        Set<String> publishers = new HashSet<>();

        for (Game game : games) {
            publishers.add(game.getPublisher());
        }

        System.out.println("\nList of all publishers: \n");
        for (String publisher : publishers) {
            System.out.println(publisher);
        }
    }

    public static void showSteamWalletBalance() {
        double balance = SteamWallet.getBalance();
        System.out.println("Current Steam Wallet Balance: " + balance);
    }

    public static void main(String[] args) {
        if (login()) {
            adminMenu();
        } else
            System.out.println("You are not Admin!");
    }
}